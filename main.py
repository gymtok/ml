import consts
from fastapi import FastAPI, HTTPException
import pandas as pd
import numpy as np
from globalVariables import globalVariables
import networkx as nx
from tqdm import tqdm
from node2vec import Node2Vec
from pydantic import BaseModel
import uvicorn
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split

app = FastAPI()


def generate_graph():
    # generate graph by preferential attachment = a node with existing edges will have
    # higher probability to get even more edges than a node without edges or with fewer edges.
    # That is, a highly liked training is more likely to become even more popular.
    created_graph = None
    initial_number_of_liked_trainings = None
    while created_graph is None or not nx.is_connected(created_graph):
        initial_number_of_liked_trainings = np.random.randint(4, 10, 30).tolist()
        created_graph = nx.bipartite.preferential_attachment_graph(initial_number_of_liked_trainings, 0.2,
                                                                   create_using=nx.Graph())
    print(initial_number_of_liked_trainings)
    nx.write_edgelist(created_graph, "created_graph.txt", delimiter=",", data=False)

# generate_graph()


class Data(BaseModel):
    data: str

    # def __init__(self, data):
    #     self.data = data


@app.post("/initModelTraining")
def init_model_training(body: Data):
    print(111111111111, body.data)

    # G = nx.read_edgelist("created_graph.txt", create_using=nx.Graph(), delimiter=",")
    # edges: [(user, training)]
    edges = [edges_as_str.split(",") for edges_as_str in body.data.split("\n")]
    node_list_1 = list(set([edge[0] for edge in edges]))
    node_list_2 = list(set([edge[1] for edge in edges]))
    G = nx.from_edgelist(edges, create_using=nx.Graph())
    # print("Nodes of G", list(G.nodes))
    # print("Edges of G", list(G.edges))
    print("Degrees of users:", list(nx.degree(G, node_list_1)))
    print("Degrees of trainings:", list(nx.degree(G, node_list_2)))

    if not nx.is_bipartite(G):
        # Graph cannot be split into two sides
        raise ValueError("The created graph is not bipartite!")

    # print("nodes_list_1:\n", node_list_1)
    # print("nodes_list_2:\n", node_list_2)

    all_unconnected_pairs = []

    # traverse adjacency matrix
    for i in tqdm(node_list_1):
        for j in node_list_2:
            if not G.has_edge(i, j):
                all_unconnected_pairs.append([i, j])

    node_1_unlinked = [i[0] for i in all_unconnected_pairs]
    node_2_unlinked = [i[1] for i in all_unconnected_pairs]

    data = pd.DataFrame({'node_1': node_1_unlinked,
                         'node_2': node_2_unlinked})

    # add target variable 'link'
    data['link'] = 0

    initial_node_count = len(G.nodes)

    edges_df = pd.DataFrame({"node_1": [i[0] for i in G.edges], "node_2": [i[1] for i in G.edges]})
    fb_df_temp = edges_df.copy()

    # empty list to store removable links
    omissible_links_index = []

    for i in tqdm(edges_df.index.values):

        # remove a node pair and build a new graph
        G_temp = nx.from_pandas_edgelist(fb_df_temp.drop(index=i), "node_1", "node_2", create_using=nx.Graph())

        # check the number of nodes is same
        if len(G_temp.nodes) == initial_node_count:
            omissible_links_index.append(i)
            fb_df_temp = fb_df_temp.drop(index=i)

    print("Count of existing edges dropped for training:", len(omissible_links_index))

    fb_df_ghost = edges_df.loc[omissible_links_index]

    # add the target variable 'link'
    fb_df_ghost['link'] = 1

    data = data.append(fb_df_ghost[['node_1', 'node_2', 'link']], ignore_index=True)
    # print(data['link'].value_counts())

    fb_df_partial = edges_df.drop(index=fb_df_ghost.index.values)

    # build graph
    G_data = nx.from_pandas_edgelist(fb_df_partial, "node_1", "node_2", create_using=nx.Graph())

    node2vec = Node2Vec(G_data, dimensions=20, walk_length=6, num_walks=80)

    # train node2vec model
    n2w_model = node2vec.fit(window=5, min_count=1)

    x = {f"{i}_{j}": (n2w_model[str(i)] + n2w_model[str(j)]) for i, j in zip(data['node_1'], data['node_2'])}

    indices = {index: key for index, key in enumerate(x.keys())}

    x_mat = np.array([x[indices[i]] for i in indices.keys()])

    ind_array = list(indices.keys())
    missing_edges_indices = ind_array[:len(all_unconnected_pairs)]
    existing_edges_indices = ind_array[len(all_unconnected_pairs):]
    np.random.shuffle(missing_edges_indices)
    np.random.shuffle(existing_edges_indices)

    train_indices = missing_edges_indices[:int(0.7 * len(missing_edges_indices))] + \
        existing_edges_indices[:int(0.7 * len(existing_edges_indices))]
    test_indices = missing_edges_indices[int(0.7 * len(missing_edges_indices)):] + \
        existing_edges_indices[int(0.7 * len(existing_edges_indices)):]

    xtrain = x_mat[train_indices, :]
    xtest = x_mat[test_indices, :]
    ytrain = data['link'][train_indices]

    # xtrain, xtest, ytrain, ytest = train_test_split(x_mat, data['link'],
    #                                                 test_size=0.3,
    #                                                 random_state=35)

    lr = LogisticRegression(class_weight="balanced")

    lr.fit(xtrain, ytrain)

    predictions = lr.predict_proba(xtest)
    # print(roc_auc_score(ytest, predictions[:, 1]))

    edges_and_predictions = np.vstack(
        ([indices[i] for i in test_indices],
         predictions[:, 1])).transpose()

    # print(edges_and_predictions)

    # build graph
    whole_graph_node2vec = Node2Vec(G, dimensions=20, walk_length=6, num_walks=80)

    # train node2vec model
    whole_graph_n2w_model = whole_graph_node2vec.fit(window=5, min_count=1)

    globalVariables.update_n2v_model(whole_graph_n2w_model)
    globalVariables.update_learning_model(lr)
    globalVariables.update_graph(G)
    globalVariables.update_node_sides(node_list_1, node_list_2)

    return edges_and_predictions


def get_challenges_to_recommend(userId):
    users_side, trainings_side = globalVariables.users_side, globalVariables.trainings_side
    if userId not in users_side:
        raise HTTPException(status_code=404, detail="User is not exist")
    return [non_neighbor for non_neighbor in trainings_side
            if not globalVariables.graph.has_edge(userId, non_neighbor)]


def get_features(user_id, all_challenges_to_recommend):
    all_node_features = globalVariables.n2v_model
    return np.array(
        [all_node_features[user_id] + all_node_features[challenge] for challenge in all_challenges_to_recommend])


@app.get("/recommendedChallenges/{userId}")
def read_item(userId: str):
    print(2222222222222222222222)
    if globalVariables.n2v_model is None and globalVariables.learning_model is None:
        raise HTTPException(status_code=400, detail="Model is not initialized. call /initModel first to init the model")

    all_challenges_to_recommend = get_challenges_to_recommend(userId)

    challenges_feature_matrix = get_features(userId, all_challenges_to_recommend)

    challenges_predictions = globalVariables.learning_model.predict_proba(challenges_feature_matrix)

    challenges_and_predictions = np.vstack(
        (all_challenges_to_recommend, challenges_predictions[:, 1])).transpose()

    return challenges_and_predictions


# TODO: {User_id}/recommend_challenges:
# 1. load lr and x from wherever.
# 2. get all missing edges for user_id, missing_train_ids.
# 3. get x vectors "user_id _ missing_train_id" - this is a test matrix
# 4. lr.predict_proba on x (run from line 141 to return)

# init_model_training()

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=7000)