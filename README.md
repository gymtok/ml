Instructions:
============
0. install python3 and pip3
1. open PyCharm Settings => Project Interpreter => Show All => Add Interpreter
2. Select New Envirnoment (Virtualenv Envirnoment) 
3. Make sure the Location is ...\ml\venv
4. Select Base Interpreter and click ok
5. cd to the project directory and then activate your virtual envirnoment - 
in windows - `./venv/Scripts/activate` 
in git bash/linux - `source ./venv/Scripts/activate`
6. `pip3 install -r requirements.txt` 
7. At the top bar where you run the project go to Edit Configurations find `ml\main.py` file as the script path
8. Run the project
 
requirements:
--------
- python3 
- pip3
- Microsoft Visual C++ 14.0(at least 14)
