isDefinedGlobalVariables = False


class GlobalVariablesClass:
    _instance = None
    learning_model = None
    n2v_model = None
    graph = None
    users_side = None
    trainings_side = None

    def update_n2v_model(self, new_n2v_model):
        self.n2v_model = new_n2v_model

    def update_learning_model(self, new_learning_model):
        self.learning_model = new_learning_model

    def update_graph(self, new_graph):
        self.graph = new_graph

    def update_node_sides(self, users_side, trainings_side):
        self.users_side = users_side
        self.trainings_side = trainings_side


def GlobalVariables():
    if not isDefinedGlobalVariables:
        GlobalVariables._instance = GlobalVariablesClass()
    return GlobalVariables._instance


globalVariables = GlobalVariables()
